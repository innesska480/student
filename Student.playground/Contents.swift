import UIKit


struct Student {
    
    var name : String
    var number : String
    var subject : [String]
    var rating: Float
    var additionalSubject: [String?]
    enum formOfEducation : String {
        case offline
        case online
    }
    public var enumValue :formOfEducation


    init(name:String, number: String, subject: [String], additionalSubject: [String?], rating: Float) {
        self.name = name
        self.number = number
        self.subject = []
        self.additionalSubject = []
        self.rating = rating
        self.enumValue = .offline
        }
    
    mutating func addSubject (subject: String) {
        if self.subject.count < 5 {
            self.subject.append(subject)
        }
    }
    
    mutating func removeSubject (removeSubject: String) {
        if self.subject.count >= 1 {
            self.subject.removeAll(where: {removeSubject.contains($0)})
        }
    }
    
    mutating func addAdditionalSubject(addSubject : String) {
        if self.additionalSubject.count < 10 {
            self.additionalSubject.append(addSubject)
        }
    }
    
    mutating func removeAdditionalSubject (removeSubject : String) {
        self.additionalSubject.removeAll(where: {removeSubject.contains($0!)})
    }
}

var allStudents: [Student] = []

func arrCreator (student:Student) {
    allStudents.append(student)
}

var studentKate = Student(
    name: "Kate",
    number: "0934567898",
    subject: ["Swift"],
    additionalSubject: [],
    rating: 98.3
)

studentKate.addSubject(subject: "CSS")
studentKate.addSubject(subject: "HTML")
studentKate.addSubject(subject: "PHP")
studentKate.addAdditionalSubject(addSubject: "Music")
studentKate.enumValue.self = .online

var studentInna = Student(
    name: "Inna",
    number: "0951112233",
    subject: ["Computer Science"],
    additionalSubject: [""],
    rating: 80.5
)

studentInna.addSubject(subject: "Math")
studentInna.addSubject(subject: "Paint")
studentInna.addAdditionalSubject(addSubject: "Physics")
studentInna.addAdditionalSubject(addSubject: "Economy")
studentInna.addAdditionalSubject(addSubject: "English")
studentInna.addAdditionalSubject(addSubject: "Polish")
studentInna.addAdditionalSubject(addSubject: "Music")
studentInna.addAdditionalSubject(addSubject: "Nature Science")
studentInna.removeSubject(removeSubject: "Math")
studentInna.enumValue.self = .offline

var studentVasia = Student(
    name: "Vasia",
    number: "0982223344",
    subject: ["Paint"],
    additionalSubject: [""],
    rating: 67.5
)

studentVasia.addSubject(subject: "Math")
studentVasia.addSubject(subject: "Physics")
studentVasia.addSubject(subject: "Economy")
studentVasia.addAdditionalSubject(addSubject: "Music")
studentVasia.addAdditionalSubject(addSubject: "Ukrainian")
studentVasia.removeAdditionalSubject(removeSubject: "Music")
studentVasia.removeAdditionalSubject(removeSubject: "Ukrainian")
studentVasia.enumValue.self = .online

arrCreator(student: studentInna)
arrCreator(student: studentVasia)
arrCreator(student: studentKate)

var onlineFormOfEducation: [String] = []
var offline:[String] = []

for student in allStudents {
  print(student)
    if student.enumValue.self == .online {
        onlineFormOfEducation.append(student.name)
    } else {
        offline.append(student.name)
    }
}

 
let offlineCounter = offline.count
let onlineCounter = onlineFormOfEducation.count

var studendsWithoutAddSubjects : [String?] = []
for addSubject in allStudents {
    if addSubject.additionalSubject.self == [] {
        studendsWithoutAddSubjects.append(addSubject.name)
    }
}

var arrayOfRating :[Float] = []
for rating in allStudents {
    arrayOfRating.append(rating.rating)
    }
print(arrayOfRating)
var avarage = arrayOfRating.reduce(0.0){
    return $0 + $1/Float(arrayOfRating.count)
}
print("Avarage rating is \(avarage)")
print("The rating of first student is \(allStudents[0].rating)")
print("\(offlineCounter) Student(s) on offline form of Education ")
print("\(onlineCounter) Student(s) on online form of Education")
print("Student \(studendsWithoutAddSubjects) has/have no ane additional subjects")
